'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var MessageSchema = new Schema({
  conversationId: {
    type: String,
    required: 'The message needs a conversationId'   
  },
  timestamp: {
    type: Date,
    default: Date.now   
  },
  from: {
    type: String,
    required: 'The message needs a from id'
  },
  to: {
    type: String,
    required: 'The message needs a to id'
  },
  text: {
    type: String,
    required: 'The message needs a text'
  },
  Created_date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('Messages', MessageSchema);