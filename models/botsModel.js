'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var BotSchema = new Schema({
  id: {
    type: String,
    required: 'The bot needs an Id'   
  },
  name: {
    type: String,
    required: 'The bot needs a name'
  },
  Created_date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('Bots', BotSchema);