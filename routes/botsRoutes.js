'use strict';
module.exports = function(app) {
  var bot = require('../controllers/botsController');

  // bot Routes
  app.route('/bots')
    .get(bot.list)
    .post(bot.create);

  app.route('/bots/:botId')
    .get(bot.show)
    .put(bot.update)
    .delete(bot.delete);
};
