'use strict';
module.exports = function(app) {
  var message = require('../controllers/messagesController');

  // message Routes
  app.route('/messages')
    .get(message.listByConversationId) //verify if conversationId parameter is present
    .post(message.create);

  app.route('/messages/:messageId')
    .get(bot.show);
};
