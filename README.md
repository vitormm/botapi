﻿# Desafio - Vitor Matta

Este repositório contém o código da solução utilizada para criar uma API REST para o desafio da Telefônica.
A Api consiste em uma API REST criada em NodeJs, utilizando-se Mongodb como banco de dados. A escolha do mongo se deu pelo fato de ser altamente escalável e por imaginar que a api irá receber uma quantidade grande de requests, daí a necessidade de se utilizar uma base de dados voltada para documentos e não um banco relacional comum.
Para o desenvolvimento foi utilizado o express como servidor local. O sistema operacional utilizado foi o Linux Fedora 25.
Para os testes, utilizei os frameworks mocha e chai, largamente utilizados nos testes de aplicações nodejs

## Execução local

Para executar o projeto localmente, siga os seguintes passos:

 - git clone https://gitlab.com/vitormm/botapi
 - cd botapi
 - npm install
 - npm run start

Para executar os testes:

 - cd test
 - mocha

Para fazer alguns requests, podemos utilizar o postman ou curl, com as seguintes chamadas:

 - Listar bots: curl -X GET http://localhost:3000/bots 
 - Criar bot: curl -X POST http://localhost:3000/bots -H 'cache-control: no-cache' -H 'content-type: application/x-www-form-urlencoded' -d 'name=teste5&id=0e44d047-fe58-44d9-9b8b-61993f49b23d'
 - Exibir um bot: curl -X GET http://localhost:3000/bots/1227d5be-3a37-413e-8612-3adbcafd6ffb -H 'cache-control: no-cache'  -H 'content-type: application/x-www-form-urlencoded' -d id=1227d5be-3a37-413e-8612-3adbcafd6ffb
 - Atualizar um bot: curl -X PUT  http://localhost:3000/bots/1227d5be-3a37-413e-8612-3adbcafd6ffb  -H 'cache-control: no-cache'  -H 'content-type: application/x-www-form-urlencoded' -d 'name=teste4&id=61135665-e826-4107-acd8-0779c7e41906'
 - Apagar bot: curl -X DELETE http://localhost:3000/bots/1227d5be-3a37-413e-8612-3adbcafd6ffb  -H 'cache-control: no-cache'  -H 'content-type: application/x-www-form-urlencoded'  -d id=1227d5be-3a37-413e-8612-3adbcafd6ffb

## Algumas Considerações

### Deploy
Para o deploy, o que eu faria se tivesse tempo seria deploy no Heroku ou então na Amazon, utilizando dos serviços já existentes que permitem o deploy de aplicações NodeJs, como por exemplo o EC2, Elastic load balance etc. Na Amazon poderíamos usar o DynamoDB e transformar a api em uma Api Lambda. Desta forma teríamos uma escala ainda maior para nossa aplicação.

### Testes
Para os testes, utilizei mocha e chai como comentado anteriormente. Gostaria de ter conseguido testar mais e criar mais métodos de teste. Minha ideia era fazer testes de carga utilizando-se o tsung. Com o tsung podemos criar cenários de teste sofisticados, simulando usuários sazonais ou não e de forma bem completa.

### Débitos técnicos identificados
Caso tivesse mais tempo, teria feito um tratamento de erro melhor nos controllers, bem como o uso de validadores do NodeJs. Isso traria uma robustez maior para a API.

