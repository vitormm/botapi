'use strict';

var mongoose = require('mongoose'),
  Bot = mongoose.model('Bots');

exports.list = function(req, res) {
  Bot.find({}, function(err, bot) {
    if (err)
      res.send(err);
    res.json(bot);
  });
};


exports.create = function(req, res) {
  var new_bot = new Bot(req.body);
  new_bot.save(function(err, bot) {
    if (err)
      res.send(err);
    res.json(bot);
  });
};


exports.show = function(req, res) {
  Bot.find({id: req.params.botId}, function(err, bot) {
    if (err)
      res.send(err);
    res.json(bot);
  });
};


exports.update = function(req, res) {
  Bot.findOneAndUpdate({id: req.params.botId}, req.body, {new: true}, function(err, bot) {
    if (err)
      res.send(err);
    res.json(bot);
  });
};


exports.delete = function(req, res) {
  Bot.remove({
    id: req.params.botId
  }, function(err, bot) {
    if (err)
      res.send(err);
    res.json({ message: 'Bot successfully deleted' });
  });
};