'use strict';

var mongoose = require('mongoose'),
  Message = mongoose.model('Messages');

//Implement pagination with query string page and limit
exports.listByConversationId = function(req, res) {
  Message.find({conversationId: req.query.conversationId}, function(err, message) {
    if (err)
      res.send(err);
    res.json(message);
  });
};


exports.create = function(req, res) {
  var new_message = new Message(req.body);
  new_message.save(function(err, message) {
    if (err)
      res.send(err);
    res.json(message);
  });
};


exports.show = function(req, res) {
  Message.find({_id: req.params.messageId}, function(err, message) {
    if (err)
      res.send(err);
    res.json(message);
  });
};
